import { Matiere } from './Matiere';
export class InscriptionDTO {
    
        public nom : string;
    
        public prenom : string;
    
        public niveau_scolaire : string;
    
        public telephone : number;
    
        public quartier : string;
    
        public numero_rue : number;
    
        public login : string;
    
        public password : string;

        public tableauMatieres : Matiere[];
    
}