import { Repetition } from './Repetition';
export class Seance {
    
        public lecon: string;
        public lieu: string;
        public date_seance : Date;
        public heure_debut_seance : Date;
        public heure_fin_seance : Date;
        public repetition : Repetition;
    
    }