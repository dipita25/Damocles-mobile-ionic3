import { Utilisateur } from './utilisateur';
import { Seance } from './Seance';
export class FeedbackDTO {
    
        public appreciation: string;
        public heure_arrivee : number;
        public heure_arrivee_minute : number;
        public heure_depart : number;
        public heure_depart_minute : number;
        public heure_debut : number;
        public heure_debut_minute : number;
        public heure_fin : number;
        public heure_fin_minute : number;
        public auteur : Utilisateur;
        public id_auteur : number;
        public id_seance : number;
    
    }