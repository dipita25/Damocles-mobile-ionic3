import { Repetition } from './Repetition';
export class SeanceDTO {
    
        public id_apprenant : number;
        public lecon: string;
        public lieu: string;
        public date_seance : Date;
        public heure_debut_seance : number;
        public heure_debut_seance_minute : number;
        public heure_fin_seance : number;
        public heure_fin_seance_minute : number;
        public repetition : Repetition;
    
    }