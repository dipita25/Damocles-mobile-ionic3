import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { PlanningPage } from '../pages/planning/planning';
import { FeedbackPage } from '../pages/feedback/feedback';
import { InfoPage } from '../pages/info/info';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { DetailPage } from './../pages/detail/detail';
import { RegisterPage } from './../pages/register/register';
import { NewplanningPage } from './../pages/newplanning/newplanning';
import { DetailseancePage } from './../pages/detailseance/detailseance';
import { DetailseancefeedbackPage } from './../pages/detailseancefeedback/detailseancefeedback';
import { NouveaufeedbackPage } from './../pages/nouveaufeedback/nouveaufeedback';
import { HttpModule } from '@angular/http';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  declarations: [
    MyApp,
    PlanningPage,
    FeedbackPage,
    InfoPage,
    TabsPage,
    LoginPage,
    RegisterPage,
    DetailPage,
    NewplanningPage,
    DetailseancePage,
    DetailseancefeedbackPage,
    NouveaufeedbackPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PlanningPage,
    FeedbackPage,
    InfoPage,
    TabsPage,
    LoginPage,
    RegisterPage,
    DetailPage,
    NewplanningPage,
    DetailseancePage,
    DetailseancefeedbackPage,
    NouveaufeedbackPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
