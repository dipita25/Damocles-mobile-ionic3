import { NgModule } from '@angular/core';
import { LoginDtoComponent } from './login-dto/login-dto';
@NgModule({
	declarations: [LoginDtoComponent],
	imports: [],
	exports: [LoginDtoComponent]
})
export class ComponentsModule {}
