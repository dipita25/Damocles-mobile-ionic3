import { Component } from '@angular/core';

/**
 * Generated class for the LoginDtoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'login-dto',
  templateUrl: 'login-dto.html'
})
export class LoginDtoComponent {

  text: string;

  constructor() {
    console.log('Hello LoginDtoComponent Component');
    this.text = 'Hello World';
  }

}
