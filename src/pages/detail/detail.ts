import { DetailseancePage } from './../detailseance/detailseance';
import { NewplanningPage } from './../newplanning/newplanning';
import { Component } from '@angular/core';
import { Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';

/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {
  public data : any;
  public items : any;
  public repetition:any;
  public id_apprenant : any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,
    public modalCtrl: ModalController) {

      this.id_apprenant = this.navParams.get('data').id;

      console.log(this.navParams.get('data'));
      this.data = {id : this.navParams.get('data').id,nom : this.navParams.get('data').nom,
        prenom : this.navParams.get('data').prenom,classe : this.navParams.get('data').classe,
        age : this.navParams.get('data').age,etablissement : this.navParams.get('data').etablissement,
        id_parent : this.navParams.get('data').id_parent};
      console.log(this.data.nom);
  
      
          this.http.post("http://localhost:8080/Damocles/rest/SeanceEtudiant",this.data)
          .map(res => res.json())
            .subscribe(data => {
              console.log(this.navParams.get('data'));

              this.items = data;
              this.repetition = this.items[0].repetition;
              console.log(this.repetition);
              console.log(this.items);
      
            }, error => {
              console.log(error);// Error getting the data
            }
          );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailPage');
  }

  nouveau_planning(data){
    let donnees={id_apprenant :this.id_apprenant ,repetition:this.repetition}
    console.log(data.prenom);
    
    let modal = this.modalCtrl.create(NewplanningPage,{data: donnees});
    modal.present();
  }

  itemSelected(item){
    let modal = this.modalCtrl.create(DetailseancePage,{data: item});
    modal.present();
  }

}
