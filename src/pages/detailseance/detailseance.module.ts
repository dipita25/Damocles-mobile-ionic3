import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailseancePage } from './detailseance';

@NgModule({
  declarations: [
    DetailseancePage,
  ],
  imports: [
    IonicPageModule.forChild(DetailseancePage),
  ],
})
export class DetailseancePageModule {}
