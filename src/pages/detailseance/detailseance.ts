import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DetailseancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detailseance',
  templateUrl: 'detailseance.html',
})
export class DetailseancePage {

  public donnees : any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.donnees = this.navParams.get('data');
    console.log(this.donnees);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailseancePage');
    
  }
  

}
