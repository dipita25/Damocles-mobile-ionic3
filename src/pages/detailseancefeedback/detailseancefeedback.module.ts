import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailseancefeedbackPage } from './detailseancefeedback';

@NgModule({
  declarations: [
    DetailseancefeedbackPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailseancefeedbackPage),
  ],
})
export class DetailseancefeedbackPageModule {}
