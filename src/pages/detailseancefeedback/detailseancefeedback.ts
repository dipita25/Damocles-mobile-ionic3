import { NouveaufeedbackPage } from './../nouveaufeedback/nouveaufeedback';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';

/**
 * Generated class for the DetailseancefeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detailseancefeedback',
  templateUrl: 'detailseancefeedback.html',
})
export class DetailseancefeedbackPage {

  public donnees : any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
    this.donnees = this.navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailseancefeedbackPage');
  }

  nouveau_feedback(donnees){
    let modal = this.modalCtrl.create(NouveaufeedbackPage,{data: donnees});
    modal.present()
  }

}
