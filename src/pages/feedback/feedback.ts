import { DetailseancefeedbackPage } from './../detailseancefeedback/detailseancefeedback';
import { DetailseancePage } from './../detailseance/detailseance';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html'
})
export class FeedbackPage {

  public donnees : any;
  public items : any;

  constructor(public navCtrl: NavController,private storage: Storage,public http: Http) {

    this.storage.get('id_personne').then((val1) => {
      console.log(val1);
      this.donnees = val1;
      console.log(this.donnees);
      this.storage.get('type_utilisateur').then((val2) => {
        this.donnees = {id_personne : val1,type_utilisateur : val2};
        console.log(this.donnees);
        this.http.post("http://localhost:8080/Damocles/rest/feedbackRepetiteur",this.donnees)
        .map(res => res.json())
          .subscribe(data => {
            console.log(this.donnees);
            this.items = data;
    
          }, error => {
            console.log(error);// Error getting the data
          }
        );
        
      });
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  itemSelected(item){
    this.navCtrl.push(DetailseancefeedbackPage, {data: item});
  }
  
}
