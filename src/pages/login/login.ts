import { RegisterPage } from './../register/register';
import { LoginDTO } from './../../DTO/loginDTO';
import { Injectable } from '@angular/core';
import { TabsPage } from './../tabs/tabs';
import { Component } from '@angular/core';
import { Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { ToastController } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Injectable()
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public data: LoginDTO = {login:"",password:""};
  public id_personne : number;
  public type_utilisateur : string;

  constructor(public navCtrl: NavController,private storage: Storage,
    public http: Http,public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    
  }

  valider(){
    console.log("premier essai");

    this.http.post("http://localhost:8080/Damocles/rest/login", this.data)
    .map(res => res.json())
      .subscribe(data => {
        if (data.type_utilisateur == "repetiteur"){
          
                  let toast = this.toastCtrl.create({
                    message: 'connection réussie',
                    duration: 3000,
                    position : "Middle"
                  });
                  toast.present();
                  console.log(data);
                  this.type_utilisateur = data.type_utilisateur;
                  this.id_personne = data.id_personne;
                  console.log(this.id_personne);
                  this.storage.set('id_personne',this.id_personne);
                  this.storage.set('type_utilisateur',this.type_utilisateur);
                  this.storage.get('id_personne').then((val) => {
                      console.log(val);
                    });
            
            //redirection vers la page des courses
            this.navCtrl.setRoot(TabsPage);
        }
        else{
            console.log("utilisateur non trouve");
        }
        
      }, error => {
        console.log(error);// Error getting the data
      }
    );
    //this.navCtrl.setRoot(TabsPage);
  }

  goToRegister(){
    console.log("page de creation de compte");
    this.navCtrl.push(RegisterPage);
  }

}
