import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewplanningPage } from './newplanning';

@NgModule({
  declarations: [
    NewplanningPage,
  ],
  imports: [
    IonicPageModule.forChild(NewplanningPage),
  ],
})
export class NewplanningPageModule {}
