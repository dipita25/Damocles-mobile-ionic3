import { PlanningPage } from './../planning/planning';
import { Repetition } from './../../DTO/Repetition';
import { SeanceDTO } from './../../DTO/seanceDTO';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the NewplanningPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-newplanning',
  templateUrl: 'newplanning.html',
})
export class NewplanningPage {

  public id_apprenant : number;
  public repetition : Repetition;
  public heure_debut_seance : Date = new Date;
  public heure_fin_seance : Date = new Date;

  public seanceDTO : SeanceDTO ={id_apprenant:this.id_apprenant,lecon : "",lieu:"",date_seance:null,
  heure_debut_seance:null,heure_debut_seance_minute : null,heure_fin_seance:null,
  heure_fin_seance_minute : 0,repetition:this.repetition};

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage,public http: Http,public toastCtrl: ToastController) {

      this.id_apprenant =this.navParams.get('data').id_apprenant;
      this.repetition = this.navParams.get('data').repetition;

      this.seanceDTO ={id_apprenant:this.id_apprenant,lecon : "",lieu:"",date_seance:null,
      heure_debut_seance:null,heure_debut_seance_minute : null,heure_fin_seance:null,
      heure_fin_seance_minute : 0,repetition:this.repetition};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewplanningPage');
  }

  enregistrer(){

        //recuperons l'heure de debut qui est sous la forme(HH:mm)
        //pour la mettre sous la forme(yyyy-mm-ddThh:mm:ss.000Z)
        let heure_pour_debut = this.heure_debut_seance.toString().split(":");
        //cette variable sert à enlever une heure vu que cette heure est GMT
        let intermediaire = +heure_pour_debut[0];
        let heure_debut_heure = intermediaire;
        intermediaire = +heure_pour_debut[1];
        let heure_debut_minute = intermediaire;

        //recuperons l'heure de fin qui est sous la forme(HH:mm)
        //pour la mettre sous la forme(yyyy-mm-ddThh:mm:ss.000Z)
        let heure_pour_fin = this.heure_fin_seance.toString().split(":");
        //cette variable sert à enlever une heure vu que cette heure est GMT
        intermediaire = +heure_pour_fin[0];
        let heure_fin_heure = intermediaire;
        intermediaire = +heure_pour_fin[1];
        let heure_fin_minute = intermediaire;

        //mise de l'heure de debut dans le bon format 
        let heure_debut_format : Date = new Date("2018-01-01T"+ heure_debut_heure +
          ":"+ heure_debut_minute +":00.000Z");

        //mise de l'heure de fin dans le bon format 
        let heure_fin_format : Date = new Date("2018-01-01T"+ heure_fin_heure +
        ":"+ heure_fin_minute +":00.000Z");
        console.log(heure_fin_format);
        console.log(this.seanceDTO.heure_fin_seance);

        this.seanceDTO  = {id_apprenant:this.id_apprenant,lecon : this.seanceDTO.lecon
          ,lieu:this.seanceDTO.lieu,date_seance:this.seanceDTO.date_seance,
          heure_debut_seance:heure_debut_heure,heure_debut_seance_minute: heure_debut_minute,
          heure_fin_seance:heure_fin_heure,heure_fin_seance_minute : heure_fin_minute,
          repetition:this.repetition
        };
        console.log(this.seanceDTO);
   this.http.post("http://localhost:8080/Damocles/rest/newSeanceMobile", this.seanceDTO)
    .map(res => res.json())
      .subscribe(data => {

            console.log(data);
            
            //redirection vers la page des planning
            this.navCtrl.setRoot(PlanningPage);
       
        
      }, error => {
        console.log(error);// Error getting the data

        

        
      }
    );
  }

}
