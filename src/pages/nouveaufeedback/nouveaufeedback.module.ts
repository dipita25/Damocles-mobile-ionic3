import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NouveaufeedbackPage } from './nouveaufeedback';

@NgModule({
  declarations: [
    NouveaufeedbackPage,
  ],
  imports: [
    IonicPageModule.forChild(NouveaufeedbackPage),
  ],
})
export class NouveaufeedbackPageModule {}
