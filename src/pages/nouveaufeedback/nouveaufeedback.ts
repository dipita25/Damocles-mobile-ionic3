import { PlanningPage } from './../planning/planning';
import { FeedbackDTO } from './../../DTO/feedbackDTO';
import { Utilisateur } from './../../DTO/utilisateur';
import { Seance } from './../../DTO/Seance';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the NouveaufeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nouveaufeedback',
  templateUrl: 'nouveaufeedback.html',
})
export class NouveaufeedbackPage {

  public seance : Seance;
  public id_seance : number;
  public id_auteur : number;
  public auteur : Utilisateur;

  public feedbackDTO : FeedbackDTO;

  public heure_debut : Date = new Date;
  public heure_fin : Date = new Date;
  public heure_arrivee : Date = new Date;
  public heure_depart : Date = new Date;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private storage: Storage,public http: Http,public toastCtrl: ToastController) {

      this.feedbackDTO = {
        appreciation : '',heure_arrivee : 0,heure_arrivee_minute : 0,
        heure_depart : 0,heure_depart_minute: 0,heure_debut : 0,
        heure_debut_minute : 0,heure_fin : 0,heure_fin_minute : 0,auteur : this.auteur,
        id_auteur : this.id_auteur,id_seance : this.id_seance};

    console.log(this.navParams.get('data'));

    this.id_seance = this.navParams.get('data').id;
    
    this.storage.get('id_personne').then((val1) => {
      console.log(val1);
      this.id_auteur = val1;
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NouveaufeedbackPage');
  }

  enregistrer(){
    //recuperons l'heure de debut qui est sous la forme(HH:mm)
        //pour la mettre sous la forme(yyyy-mm-ddThh:mm:ss.000Z)
        let heure_pour_debut = this.heure_debut.toString().split(":");
        //cette variable sert à enlever une heure vu que cette heure est GMT
        let intermediaire = +heure_pour_debut[0];
        let heure_debut_heure = +intermediaire;
        intermediaire = +heure_pour_debut[1];
        let heure_debut_minute = intermediaire;


        //recuperons l'heure de fin qui est sous la forme(HH:mm)
        //pour la mettre sous la forme(yyyy-mm-ddThh:mm:ss.000Z)
        let heure_pour_fin = this.heure_fin.toString().split(":");
        //cette variable sert à enlever une heure vu que cette heure est GMT
        let intermediaire2 = +heure_pour_fin[0];
        let heure_fin_heure = +intermediaire2;
        intermediaire2 = +heure_pour_fin[1];
        let heure_fin_minute = intermediaire2;

        //recuperons l'heure de arrivee qui est sous la forme(HH:mm)
        //pour la mettre sous la forme(yyyy-mm-ddThh:mm:ss.000Z)
        let heure_pour_arrivee = this.heure_arrivee.toString().split(":");
        //cette variable sert à enlever une heure vu que cette heure est GMT
        let intermediaire3 = +heure_pour_arrivee[0];
        let heure_arrivee_heure = intermediaire3;
        intermediaire3 = +heure_pour_arrivee[1];
        let heure_arrivee_minute = intermediaire3;


        //recuperons l'heure de depart qui est sous la forme(HH:mm)
        //pour la mettre sous la forme(yyyy-mm-ddThh:mm:ss.000Z)
        let heure_pour_depart = this.heure_depart.toString().split(":");
        //cette variable sert à enlever une heure vu que cette heure est GMT
        let intermediaire4 = +heure_pour_depart[0];
        let heure_depart_heure = intermediaire4;
        intermediaire4 = +heure_pour_depart[1];
        let heure_depart_minute = intermediaire4;


        this.feedbackDTO = {
          appreciation : this.feedbackDTO.appreciation,heure_arrivee : heure_arrivee_heure
          ,heure_arrivee_minute : heure_arrivee_minute,
          heure_depart : heure_depart_heure,heure_depart_minute: heure_depart_minute
          ,heure_debut :heure_debut_heure,
          heure_debut_minute : heure_debut_minute,heure_fin : heure_fin_heure
          ,heure_fin_minute :heure_fin_minute,auteur : this.auteur,
          id_auteur : this.id_auteur,id_seance : this.id_seance};


      this.http.post("http://localhost:8080/Damocles/rest/enregistrerFeedbackMobile", this.feedbackDTO)
      .map(res => res.json())
        .subscribe(data => {
  
          let toast = this.toastCtrl.create({
            message: 'feedback enregistré avec succès',
            duration: 3000,
            position : "Middle"
          });
              
              //redirection vers la page des planning
              this.navCtrl.setRoot(PlanningPage);
         
          
        }, error => {
          console.log(error);// Error getting the data
        }
        );

  }
}
