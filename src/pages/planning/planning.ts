import { DetailPage } from './../detail/detail';
import { FeedbackPage } from './../feedback/feedback';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-planning',
  templateUrl: 'planning.html'
})
export class PlanningPage {

  public items : any;
  public donnees : any;
  constructor(public navCtrl: NavController,public http: Http,public toastCtrl: ToastController
  ,private storage: Storage,public navParams: NavParams) {

    this.storage.get('id_personne').then((val1) => {
      console.log(val1);
      this.donnees = val1;
      console.log(this.donnees);
      this.storage.get('type_utilisateur').then((val2) => {
        this.donnees = {id_personne : val1,type_utilisateur : val2};
        console.log(this.donnees);
        this.http.post("http://localhost:8080/Damocles/rest/mesApprenants",this.donnees)
        .map(res => res.json())
          .subscribe(data => {
            console.log(this.donnees);
            this.items = data;
    
            /*let toast = this.toastCtrl.create({
              message: 'connection réussie',
              duration: 3000,
              position : "Middle"
            });
            toast.present();*/
          }, error => {
            console.log(error);// Error getting the data
          }
        );
        
      });
    });

  }

  ionViewDidLoad() {
    
  }

  itemSelected(item){
    console.log(item);
    this.navCtrl.push(DetailPage, {data: item});
  }
}
