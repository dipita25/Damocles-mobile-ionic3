import { LoginPage } from './../login/login';
import { Matiere } from './../../DTO/Matiere';
import { InscriptionDTO } from './../../DTO/InscriptionDTO';
import { TabsPage } from './../tabs/tabs';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Injectable()
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  public matieres : [Matiere];
  public Mesmatieres : [Matiere];
  public data : {};

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
    this.http.get("http://localhost:8080/Damocles/rest/matieres")
    .map(res => res.json())
      .subscribe(data => {
        console.log(data);
        this.matieres = data;
        console.log(this.matieres);
        
      }, error => {
        console.log(error);// Error getting  data
      }
    );
  
  }

  Register(){
    this.data = {nom:"repetiteur",prenom:"repetiteur",niveau_scolaire:"BAC",telephone:25,
    quartier:"newbell",numero_rue:245,login:"repetiteur",password:"repetiteur",
    tableauMatieres:[{id:2,version:0,intitule:"Histoire"}]};

    console.log(this.matieres);
    
    this.http.post("http://localhost:8080/Damocles/rest/repetiteurs/create", this.data)
    .map(res => res.toString)
      .subscribe(data => {
        console.log("success");
      this.navCtrl.push(LoginPage);

      }, error => {
        console.log(error);// Error getting the data
      }
      );
  }

}
