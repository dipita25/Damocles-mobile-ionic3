import { FeedbackPage } from './../feedback/feedback';
import { InfoPage } from './../info/info';
import { PlanningPage } from './../planning/planning';
import { Component } from '@angular/core';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = InfoPage;
  tab2Root = PlanningPage;
  tab3Root = FeedbackPage;

  constructor() {

  }
}
